package handler

import "context"
import "net/http"

// Response of function call
type Response struct {

	// Body the body will be written back
	Body []byte

	// StatusCode needs to be populated with value such as http.StatusOK
	StatusCode int

	// Code is API-specific status code
	Code int

	// Header is optional and contains any additional headers the function response should set
	Header http.Header
}

// Request of function call
type Request struct {
	// Body is a byte array of the request body
	Body []byte
	// Header is the http header
	Header http.Header
	// QueryString is the query string of the request
	QueryString string
	// Method is the http method
	Method string
	// Request is the original http request
	Request *http.Request
	// Context is the context for OpenTracing
	Context context.Context
}

// FunctionHandler used for a serverless Go method invocation
type FunctionHandler interface {
	Handle(req Request) (Response, error)
}

func init() {

}
