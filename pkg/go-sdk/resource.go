package handler

import "time"

import "github.com/Shopify/sarama"
import "github.com/tannhauser9000/utils/pool"

// resourceStPool stores a set of resourceSt objects for resource access
type resourceStPool struct {
	init     bool
	index    *pool.IndexPool
	resource []*resourceSt
}

// resourceSt store
type resourceSt struct {
	index         int
	SyncProducer  *sarama.SyncProducer
	AsyncProducer *sarama.AsyncProducer
}

// package-scoped constants
const _sizeResourcePool = 3
const _kafkaBroker = "kafka-svc.monitor:9092"
const _kafkaKeepAlive = 300  //s
const _kafkaRefreshMeta = 60 //s
const _kafkaFullMeta = false
const _kafkaRequireAck = sarama.WaitForLocal

// package-scoped resources
var resourcePool *resourceStPool

// get returns a pointer to a resourceSt object
func get() (*resourceSt, error) {
	// to do: check producer status before return
	if resourcePool == nil || !(*resourcePool).init {
		err := initResourcePool()
		if err != nil {
			return nil, err
		}
	}
	index := (*resourcePool).index.Get()
	(*(*resourcePool).resource[index]).index = index
	return (*resourcePool).resource[index], nil
}

// Free free up a resource got from Get
func (r *resourceSt) free() {
	index := (*r).index
	(*r).cleanup()
	(*resourcePool).index.Free(index)
}

// InitResource initialize the package-scoped resource pool
func InitResource() error {
	return initResourcePool()
}

// initResourcePool initialize the package-scoped resource pool
func initResourcePool() error {
	resourcePool = &resourceStPool{
		index:    &pool.IndexPool{},
		resource: make([]*resourceSt, _sizeResourcePool),
	}
	config := sarama.NewConfig()
	(*config).Net.KeepAlive = _kafkaKeepAlive * time.Second
	(*config).Metadata.RefreshFrequency = _kafkaRefreshMeta * time.Second
	(*config).Metadata.Full = _kafkaFullMeta
	(*config).Producer.RequiredAcks = _kafkaRequireAck
	(*config).Producer.Return.Successes = true
	(*resourcePool).index.Init(_sizeResourcePool)
	for i := 0; i < _sizeResourcePool; i++ {
		p, err := sarama.NewSyncProducer([]string{_kafkaBroker}, config)
		if err != nil {
			return err
		}
		(*resourcePool).resource[i] = &resourceSt{
			index:        i,
			SyncProducer: &p,
		}
	}
	(*resourcePool).init = true
	return nil
}

// cleanup reset resources of the resourceSt object if necessary, currently do nothing
func (r *resourceSt) cleanup() {
}
