package handler

import "github.com/Shopify/sarama"

// Send sends message to Kafka in Synchronous way
func Send(topic, msg string) (int32, int64, error) {
	resource, err := get()
	if err != nil {
		return 0, 0, err
	}
	defer resource.free()
	p := (*resource).SyncProducer
	partition, offset, err := (*p).SendMessage(&sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(msg),
	})
	if err != nil {
		return 0, 0, err
	}
	return partition, offset, nil
}
